// Header Active
var header = document.getElementById("header");
var toActive = document.querySelectorAll(".toactive");
var menuDrop = document.querySelectorAll(".menudrop");

for (var i = 0; i < toActive.length; i++) {
  toActive[i].addEventListener("click", function () {
    // add class active to active page
    var current = document.getElementsByClassName("active");
    var currentMenuDrop = document.getElementsByClassName("menudrop-active");

    current[0].classList.remove("active");
    currentMenuDrop[0].classList.remove("menudrop-active");
    this.classList.add("active");

    // Add dot for active page
    for (var i = 0; i < toActive.length; i++) {
      if (toActive[i].className == "toactive active") {
        break;
      }
    }
    
    menuDrop[i].classList.add("menudrop-active");
  });
}

// Sticky Header
window.onscroll = function() {myFunction()};

var logo = document.getElementById("logo");
var logoImg = document.getElementById("logo-img");

var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
    logo.classList.add("invisible");
  } else {
    header.classList.remove("sticky");
    logo.classList.remove("invisible");
  };
};